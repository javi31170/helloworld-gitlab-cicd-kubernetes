#FROM node:6.9.2
#EXPOSE 7000
#COPY server.js .
#CMD node server.js
#-------------------
FROM node:6.9.2
WORKDIR /home
ADD . ./
RUN npm install -g http-server
CMD http-server -p 7000 ./